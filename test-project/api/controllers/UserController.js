/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req, res) {
    User.find().then(
      function(users) {
        res.send(users)
      },
      function(reason) {
        res.send(reason)
      }
    )
  },
  new: function (req, res) {
    User.create({firstName: req.allParams().firstName, lastName: req.allParams().lastName}).then(
      function (newUser) {
        res.send(newUser)
      },
      function (reason) {
        res.send(reason)        
      }
    )
  }
};

