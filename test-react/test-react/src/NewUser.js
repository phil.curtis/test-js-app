import React, {Component} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SaveUserButton from './SaveUserButton';
import './NewUser.css';

class NewUser extends Component{
  constructor() {
    super();
  }

  render() {
    return (
      <div className="new-user-container">
        <Card className="new-user-card">
          <h4>New User</h4>
          <div className="fields-container">
            <TextField className="input" floatingLabelText="First Name"/>
            <TextField className="input" floatingLabelText="Last Name"/>
            <TextField className="input" floatingLabelText="Email"/>
            <TextField className="input" floatingLabelText="Phone"/>
            <TextField className="text-box" floatingLabelText="Bio" maxLength="250" multiLine={true} rows={3} rowsMax={6}/>
          </div>
        </Card>
        <div className="save-user">
          <SaveUserButton></SaveUserButton>
        </div>
      </div>
    )
  }
}

export default NewUser;
