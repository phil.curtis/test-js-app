import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import UserContainer from './UserContainer';
import NewUser from './NewUser';


class App extends Component {
  render() {
    injectTapEventPlugin();
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <MuiThemeProvider>
          <Router history={Router.hashHistory}>
            <div>
              <Route exact path="/" component={UserContainer}></Route>
              <Route path="/new" component={NewUser}></Route>
            </div>
          </Router>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
