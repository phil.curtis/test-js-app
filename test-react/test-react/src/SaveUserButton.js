import React, {Component} from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentSave from 'material-ui/svg-icons/content/save';
import './SaveUserButton.css';

class SaveUserButton extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <FloatingActionButton secondary={true}>
        <ContentSave />
      </FloatingActionButton>
    )
  }
}

export default SaveUserButton;
