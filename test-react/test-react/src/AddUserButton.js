import React, {Component} from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import './AddUserButton.css';

class AddUserButton extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <FloatingActionButton secondary={true}>
        <ContentAdd />
      </FloatingActionButton>
    )
  }
}

export default AddUserButton;
