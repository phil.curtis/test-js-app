import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import './UserContainer.css';
import User from './User.js';
import AddUserButton from './AddUserButton';
import NewUser from './NewUser';

class UserContainer extends Component {
  constructor(props) {
    super()
    this.state = {users: []}
  }

  componentDidMount() {
    fetch(
      'http://localhost:1337/users'
    )
    .then(result => result.json())
    .then(users => this.setState({users}))
  }

  render() {
    return  (
      <div className="user-container">
        <ul>
          {this.state.users.map((user) => <User firstName={user.firstName} lastName={user.lastName} id={user.id}/>)}
        </ul>
        <div className="add-user">
          <Link to="/new">
            <AddUserButton>
            </AddUserButton>
          </Link>
        </div>

      </div>
      )
  }
}

export default UserContainer;
