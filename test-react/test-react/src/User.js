import React, { Component } from 'react';
import './User.css'
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

class User extends Component {
  constructor(props) {
    super(props)
    this.key = props.id
  }
  render() {
    return (
      <li key={this.key} className="user-card">
        <Card>
          <CardHeader title={`${this.props.firstName} ${this.props.lastName}`} subtitle={"Some title"} actAsExpander={true} showExpandableButton={true}/>
          <CardActions>
            <FlatButton label={"View Profile"}/>
            <FlatButton label={"Message"}/>
          </CardActions>
          <CardText expandable={true}>
            In 2009 I was donating mannequins in Gainesville, FL.
            My current pet project is getting my feet wet with childrens books in Cuba.
            Spent 2001-2004 marketing Mr. Potato Heads in Minneapolis, MN.
            At the moment I'm consulting about banjos in Naples, FL.
            Spent 2002-2007 testing the market for human hair in New York, NY.
            Spoke at an international conference about selling wooden tops in Ohio.
          </CardText>
        </Card>
      </li>
    )
  }
}

export default User;
